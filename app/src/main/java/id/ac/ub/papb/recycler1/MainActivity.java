package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rv1;
    public static String TAG = "RV1";
    private ArrayList<Mahasiswa> data = new ArrayList<>();
    private MahasiswaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
        adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        Button button = findViewById(R.id.bt1);
        button.setOnClickListener(this);
        }

    @Override
    public void onClick(View view) {
        String newNama = ((EditText) findViewById(R.id.editTextTextPersonName2)).getText().toString();
        String newNim = ((EditText) findViewById(R.id.etNim)).getText().toString();

        if (!newNama.isEmpty() && !newNim.isEmpty()) {
            Mahasiswa newMahasiswa = new Mahasiswa(newNim, newNama);
            data.add(newMahasiswa);
            adapter.notifyDataSetChanged();
            ((EditText) findViewById(R.id.editTextTextPersonName2)).setText("");
            ((EditText) findViewById(R.id.etNim)).setText("");
        }
    }
}